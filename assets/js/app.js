$(document).ready(function() {
  $(window).scroll(function() {
    var elem = $('.navbar'),
      top = $(this).scrollTop();

    if (top > $('.m-header').height() + 10) {
      elem.addClass('onscrolling navbar-fixed-top');
    } else {
      elem.removeClass('onscrolling navbar-fixed-top');
    }
  });

  $('.menu-js').on("click", function (event) {
    event.preventDefault();
    var id = $(this).attr('href'),
      top = $(id).offset().top - 50;
    $('body,html').animate({scrollTop: top}, 500);
  });


  $('#moreModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget),
      modal = $(this);
    modal.find('.modal-title').text(button.data('title'));
    modal.find('.modal-subtitle').text(button.data('subtitle') || '');
    modal.find('.modal-body #moreModalContent').html($(button.data('content')));
  });

  var mapCanvas = document.getElementById('map');
  if (mapCanvas) {
    initMap();
  }

  function initMap() {
    var mapOptions = {
        center: new google.maps.LatLng(50.912584, 34.801781),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      },
      map = new google.maps.Map(mapCanvas, mapOptions),
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(50.912584, 34.801781),
        map: map
      });
  }
});
